QT -= gui
QT += multimedia

CONFIG += c++11
CONFIG -= app_bundle

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        file.cpp \
        main.cpp \
        wav.cpp

HEADERS += \
    file.h \
    wav.h
